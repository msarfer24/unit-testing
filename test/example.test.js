const { expect } = require('chai')
const { add, sub, multiply, divide} = require('../src/mylib')

describe("My unit tests", () => {

  before(() => {})

  it("Adds 2 + 2 and equals 4", () => {
    expect(add(2,2)).to.equals(4)
  })

  it("Sub 2 - 2 and equals 0", () => {
    expect(sub(2,2)).to.equals(0)
  })

  it("Multiplies  2 * 2 and equals 4", () => {
    expect(multiply(2,2)).to.equals(4)
  })

  it("Divides  2 / 2 and equals 1", () => {
    expect(divide(2,2)).to.equals(1)
  })

  it("Fails if divisor is 0", () =>  {
    // Expect something to throw an error.
    expect(() => divide(9,0)).to.throw()
  })

  after(() => {})
})