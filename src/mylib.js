/**
 * Basic arithmetic operations
 */
const mylib = {
  /** Arrow functions that add two numbers 
   * @param {number} a
   * @param {number} b
   * @returns {number} Sum of a and b
  */
  add: (a, b) =>  a + b,

  /** Arrow functions that subtraction two numbers 
   * @param {number} a
   * @param {number} b
   * @returns {number} Substraction of a and b
  */
  sub: (a, b) => a - b,

  /** Arrow functions that divide two numbers 
   * @param {number} dividend
   * @param {number} divisor
   * @returns {number} division by the dividend by the divisor
  */
  divide: (dividend, divisor) => {
    if (divisor === 0) throw new Error('Can not divide a number by zero.')
    
    return dividend / divisor
  },

  /** Arrow functions that multiplies two numbers 
   * @param {number} a
   * @param {number} b
   * @returns {number} Product of a and b
  */
  multiply: (a, b) => a * b,
}

module.exports = mylib